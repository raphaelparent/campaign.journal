// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
    devtools: { enabled: true },
    ssr: false,
    css: ["~/assets/css/tailwind.css"],
    vite: {
        vue: {
            script: {
                defineModel: true,
            },
        },
    },
    modules: [
        "@nuxtjs/tailwindcss",
        "@nuxtjs/eslint-module",
        "@nuxtjs/google-fonts",
        "@nuxtjs/i18n",
        "@vueuse/nuxt",
        "@pinia/nuxt",
        "dayjs-nuxt",
        "nuxt-lodash",
    ],
    googleFonts: {
        families: {
            Afacad: true,
            Spectral: true,
        },
    },
    i18n: {
        vueI18n: "~/i18n.config.ts",
    },
    lodash: {
        prefix: "_",
        upperAfterPrefix: false,
    },
});
