import { useStorage } from "@vueuse/core";

/**
 * Type import
 */
import { defaultSettings } from "~/types/settings";
import type { ISettings } from "~/types/settings";

export const useSettingsStore = defineStore("settings", () => {
    /**
     * Local storage
     */
    const settingsStorage = useStorage<ISettings>("settings", defaultSettings);

    return {
        general: settingsStorage,
    };
});
