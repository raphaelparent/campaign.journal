import { useStorage } from "@vueuse/core";

/**
 * Type import
 */
import type { IQuest } from "~/types/quest";

export const useQuestsStore = defineStore("quests", () => {
    /**
     * Local storage
     */
    const questsStorage = useStorage<IQuest[]>("quests", []);

    return {
        items: questsStorage,
    };
});
