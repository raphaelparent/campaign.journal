import { useStorage } from "@vueuse/core";

/**
 * Type import
 */
import type { INpc } from "~/types/npc";

export const useNpcsStore = defineStore("npcs", () => {
    /**
     * Local storage
     */
    const npcsStorage = useStorage<INpc[]>("npcs", []);

    return {
        items: npcsStorage,
    };
});
