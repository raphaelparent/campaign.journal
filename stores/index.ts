import { useStorage } from "@vueuse/core";

/**
 * Type import
 */
import { emptyQuest } from "~/types/quest";
import type { IQuest } from "~/types/quest";
import { emptyNpc } from "~/types/npc";
import type { INpc } from "~/types/npc";
import { emptyFaction } from "~/types/faction";
import type { IFaction } from "~/types/faction";
import type { IIndexItem } from "~/types/index";
import type { SectionName } from "~/types/unions";

export const useIndexStore = defineStore("indexStore", () => {
    /**
     * Local storage
     */
    const indexStorage = useStorage<IIndexItem[]>("index", []);
    const quests = useQuestsStore();
    const npcs = useNpcsStore();
    const factions = useFactionsStore();

    /**
     * Index actions
     */
    const actions = {
        bookmark: (newItem: IIndexItem): boolean => {
            const itemPosition = indexStorage.value.findIndex((i) => i.id === newItem.id);

            if (itemPosition >= 0) {
                indexStorage.value.splice(itemPosition, 1);
                return false;
            } else {
                indexStorage.value.unshift({ ...newItem });
                return true;
            }
        },
        create: (section: SectionName): IQuest | INpc | IFaction | null => {
            if (section === "quests") {
                const newQuest = { ...emptyQuest() };
                quests.items.push(newQuest);
                return newQuest;
            } else if (section === "npcs") {
                const newNpc = { ...emptyNpc() };
                npcs.items.push(newNpc);
                return newNpc;
            } else if (section === "factions") {
                const newFaction = { ...emptyFaction() };
                factions.items.push(newFaction);
                return newFaction;
            } else {
                console.warn(`Create section ${section} is not yet supported`);
            }

            return null;
        },
    };

    return {
        items: indexStorage,
        actions,
    };
});
