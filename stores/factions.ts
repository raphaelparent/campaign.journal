import { useStorage } from "@vueuse/core";

/**
 * Type import
 */
import type { IFaction } from "~/types/faction";

export const useFactionsStore = defineStore("factions", () => {
    /**
     * Local storage
     */
    const factionsStorage = useStorage<IFaction[]>("factions", []);

    return {
        items: factionsStorage,
    };
});
