/* eslint-env node */
require("@rushstack/eslint-patch/modern-module-resolution");

module.exports = {
    root: true,
    rules: {
        indent: ["error", 4, { SwitchCase: 1 }],
        "prettier/prettier": [
            "error",
            {
                tabWidth: 4,
                printWidth: 120,
                bracketSameLine: true,
                htmlWhitespaceSensitivity: "ignore",
            },
        ],
        "vue/no-setup-props-destructure": ["off", 0],
        "no-console": ["warn", { allow: ["warn", "error"] }],
    },
    extends: [
        "@nuxtjs/eslint-config-typescript",
        "eslint:recommended",
        "@vue/eslint-config-typescript/recommended",
        "@vue/eslint-config-prettier",
        "prettier",
    ],
    env: {
        "vue/setup-compiler-macros": true,
    },
};
