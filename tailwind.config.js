/** @type {import('tailwindcss').Config} */
export default {
    content: [],
    theme: {
        extend: {
            fontFamily: {
                sans: ["Afacad", "Helvetiva", "Arial"],
                serif: ["Spectral", "Times New Roman"],
            },
        },
    },
    plugins: [],
};
