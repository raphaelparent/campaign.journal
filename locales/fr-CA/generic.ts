export default {
    journal: "Journal",
    happenedOn: "Ça c'est passé le jour",
    happenedDaysAgo: "alors c'était il y a {days} jours",
    placeholder: {
        quickfilter: "Recherche rapide",
    },
};
