export default {
    title: "Quête | Quêtes",
    noName: "Une quête sans nom",
    emptyList: "Aucune quête trouvée.",
    label: {
        introduction: "Introduction",
        rewards: "Récompense(s)",
        milestones: "Étapes importantes",
        progression: "Progression des personnages",
        development: "Développement général",
    },
    placeholder: {
        introduction: "Courte introduction de la quête",
        rewards: "Item, connaissance, pièces, etc.",
        milestones: "Courte description de l'étape",
        progression: "Entrée de progrès",
        development: "Entrée de développement",
    },
};
