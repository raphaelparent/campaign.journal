import generic from "~/locales/fr-CA/generic";
import sidebar from "~/locales/fr-CA/sidebar";
import settings from "~/locales/fr-CA/settings";
import quests from "~/locales/fr-CA/quests";
import npcs from "~/locales/fr-CA/npcs";
import factions from "~/locales/fr-CA/factions";

export default { quests, npcs, factions, generic, sidebar, settings };
