export default {
    title: "Faction | Factions",
    noName: "Faction sans nom",
    emptyList: "Aucune faction trouvée.",
    label: {
        description: "Description",
        details: "Détails",
        goals: "Objectifs et ambitions",
        encounters: "Rencontres",
        development: "Développement de l'organisation",
    },
    placeholder: {
        description: "Courte description de la faction",
        details: "Remarquable, cachés, secrets, relations, etc.",
        goals: "Description de l'objectif / ambition",
        encounters: "Entrée de rencontre",
        development: "Entrée de développement",
    },
};
