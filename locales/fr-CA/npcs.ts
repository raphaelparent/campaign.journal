export default {
    title: "Personnage non-joueur | Personnages non-joueur",
    noName: "NPC sans nom",
    emptyList: "Aucun personnage non-joueur trouvé.",
    label: {
        description: "Description",
        details: "Détails",
        goals: "Objectifs et ambitions",
        encounters: "Rencontres",
        development: "Développement personnel",
    },
    placeholder: {
        description: "Courte description du personnage non-joueur",
        details: "Remarquable, cachés, secrets, relations, etc.",
        goals: "Description de l'objectif / ambition",
        encounters: "Entrée de rencontre",
        development: "Entrée de de développement personnel",
    },
};
