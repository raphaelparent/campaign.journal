export default {
    journal: "Journal",
    happenedOn: "It happened on day",
    happenedDaysAgo: "so it's been {days} days",
    placeholder: {
        quickfilter: "Quick filter",
    },
};
