export default {
    title: "Non-player character | Non-player characters",
    noName: "Unnamed NPC",
    emptyList: "No non-player character found.",
    label: {
        description: "Description",
        details: "Details",
        goals: "Goals and ambitons",
        encounters: "Encounters",
        development: "Personal development",
    },
    placeholder: {
        description: "Short non-player character description",
        details: "Remarkable, hidden, secret, relation, etc.",
        goals: "Goal / ambition description",
        encounters: "Encounter entry",
        development: "Personal development entry",
    },
};
