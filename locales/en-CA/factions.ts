export default {
    title: "Faction | Factions",
    noName: "Unnamed faction",
    emptyList: "No faction found.",
    label: {
        description: "Description",
        details: "Details",
        goals: "Goals and ambitons",
        encounters: "Encounters",
        development: "Development",
    },
    placeholder: {
        description: "Short faction description",
        details: "Remarkable, hidden, secret, relation, etc.",
        goals: "Goal / ambition description",
        encounters: "Encounter entry",
        development: "Development entry",
    },
};
