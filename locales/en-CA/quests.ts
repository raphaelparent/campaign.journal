export default {
    title: "Quest | Quests",
    noName: "Unnamed quest",
    emptyList: "No quest found.",
    label: {
        introduction: "Introduction",
        rewards: "Reward(s)",
        milestones: "Milestones",
        progression: "Character progression",
        development: "General development",
    },
    placeholder: {
        introduction: "Short quest introduction",
        rewards: "Item, knowledge, coins, etc.",
        milestones: "Short milestone description",
        progression: "Progression entry",
        development: "Development entry",
    },
};
