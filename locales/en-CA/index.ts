import generic from "~/locales/en-CA/generic";
import sidebar from "~/locales/en-CA/sidebar";
import settings from "~/locales/en-CA/settings";
import quests from "~/locales/en-CA/quests";
import npcs from "~/locales/en-CA/npcs";
import factions from "~/locales/en-CA/factions";

export default { quests, npcs, factions, generic, sidebar, settings };
