import frCA from "~/locales/fr-CA/index";
import enCA from "~/locales/en-CA/index";

export default defineI18nConfig(() => {
    const currentLocale = process.client ? localStorage.getItem("locale") : "fr-CA";

    return {
        legacy: false,
        locale: currentLocale || "fr-CA",
        messages: {
            "en-CA": {
                "en-CA": "English",
                "fr-CA": "Français",
                ...enCA,
            },
            "fr-CA": {
                "en-CA": "English",
                "fr-CA": "Français",
                ...frCA,
            },
        },
    };
});
