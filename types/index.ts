import type { SectionName } from "~/types/unions";

export interface IIndexItem {
    id: string;
    createdAt: string;
    name: string;
    section: SectionName;
}
