export interface ISettings {
    currentDay: string;
    currentWeather: string;
}

export const defaultSettings: ISettings = {
    currentDay: "",
    currentWeather: "",
};
