import { v4 as uuid } from "uuid";
import type { Repeater, TimeTrackingRepeater } from "@/types/fields";
import type { ISection } from "~/types/base";

export interface INpc extends ISection {
    description: string;
    details: Repeater;
    goals: Repeater;
    encounters: TimeTrackingRepeater;
    development: TimeTrackingRepeater;
}

export const emptyNpc = (): INpc => {
    const dayjs = useDayjs();

    return {
        id: uuid(),
        slug: "",
        createdAt: dayjs().format("YYYY-MM-DD HH:mm:ss"),
        bookmarked: false,
        archived: false,
        name: "",
        description: "",
        details: [],
        goals: [],
        encounters: [],
        development: [],
    };
};
