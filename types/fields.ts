interface ITimeTrackingRepeater {
    day: string;
    text: string;
}

interface IRelation {
    id: number;
    type: string;
    name: string;
}

export type Name = string;
export type Description = string;
export type Repeater = string[];
export type TimeTrackingRepeater = ITimeTrackingRepeater[];
export type Relations = IRelation[];
