export interface ISection {
    id: string;
    slug: string;
    createdAt: string;
    bookmarked: boolean;
    archived: boolean;
    name: string;
}
