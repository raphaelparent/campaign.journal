import { v4 as uuid } from "uuid";
import type { Repeater, TimeTrackingRepeater } from "~/types/fields";
import type { ISection } from "~/types/base";

export interface IQuest extends ISection {
    description: string;
    rewards: Repeater;
    milestones: Repeater;
    progression: TimeTrackingRepeater;
    development: TimeTrackingRepeater;
}

export const emptyQuest = (): IQuest => {
    const dayjs = useDayjs();

    return {
        id: uuid(),
        slug: "",
        createdAt: dayjs().format("YYYY-MM-DD HH:mm:ss"),
        bookmarked: false,
        archived: false,
        name: "",
        description: "",
        rewards: [],
        milestones: [],
        progression: [],
        development: [],
    };
};
