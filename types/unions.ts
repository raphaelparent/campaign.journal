export type Locale = "fr-CA" | "en-CA";
export const locales: Locale[] = ["fr-CA", "en-CA"];

export type SectionName = "quests" | "npcs" | "factions";

export type IconName =
    | "IconMonster"
    | "IconPlus"
    | "IconMinus"
    | "IconRumours"
    | "IconJournal"
    | "IconQuests"
    | "IconBookmark"
    | "IconNote"
    | "IconKebab"
    | "IconQuestion"
    | "IconFactions"
    | "IconArchive"
    | "IconLink"
    | "IconArrowRight"
    | "IconArrowLeft"
    | "IconCaretDown"
    | "IconLocation"
    | "IconNpcs"
    | "IconEdit"
    | "IconCollapse"
    | "IconList"
    | "IconExpand";

export const sections: { name: SectionName; icon: IconName }[] = [
    { name: "quests", icon: "IconQuests" },
    { name: "npcs", icon: "IconNpcs" },
    { name: "factions", icon: "IconFactions" },
];
