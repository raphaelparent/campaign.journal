import {
    PhSkull,
    PhPlus,
    PhMinus,
    PhMegaphone,
    PhNotebook,
    PhBookBookmark,
    PhBookmarkSimple,
    PhNote,
    PhDotsThreeVertical,
    PhQuestion,
    PhFlag,
    PhReceiptX,
    PhLink,
    PhArrowRight,
    PhArrowLeft,
    PhCaretDown,
    PhMapTrifold,
    PhPerson,
    PhPencilSimple,
    PhArrowsInSimple,
    PhArrowsOutSimple,
    PhListDashes,
} from "@phosphor-icons/vue";

export default defineNuxtPlugin((nuxtApp) => {
    nuxtApp.vueApp.component("IconArchive", PhReceiptX);
    nuxtApp.vueApp.component("IconCollapse", PhArrowsInSimple);
    nuxtApp.vueApp.component("IconEdit", PhPencilSimple);
    nuxtApp.vueApp.component("IconBookmark", PhBookmarkSimple);
    nuxtApp.vueApp.component("IconMonster", PhSkull);
    nuxtApp.vueApp.component("IconPlus", PhPlus);
    nuxtApp.vueApp.component("IconMinus", PhMinus);
    nuxtApp.vueApp.component("IconRumours", PhMegaphone);
    nuxtApp.vueApp.component("IconJournal", PhNotebook);
    nuxtApp.vueApp.component("IconQuests", PhBookBookmark);
    nuxtApp.vueApp.component("IconNote", PhNote);
    nuxtApp.vueApp.component("IconKebab", PhDotsThreeVertical);
    nuxtApp.vueApp.component("IconQuestion", PhQuestion);
    nuxtApp.vueApp.component("IconFactions", PhFlag);
    nuxtApp.vueApp.component("IconLink", PhLink);
    nuxtApp.vueApp.component("IconCaretDown", PhCaretDown);
    nuxtApp.vueApp.component("IconArrowRight", PhArrowRight);
    nuxtApp.vueApp.component("IconArrowLeft", PhArrowLeft);
    nuxtApp.vueApp.component("IconLocation", PhMapTrifold);
    nuxtApp.vueApp.component("IconNpcs", PhPerson);
    nuxtApp.vueApp.component("IconExpand", PhArrowsOutSimple);
    nuxtApp.vueApp.component("IconList", PhListDashes);
});
